
#include "hacklib/PatternScanner.h"
#include "hacklib/Logging.h"
#include "hacklib/Hooker.h"

#include <codecvt>
#include <locale>

#define ASSIGN_HOOK(h, c) if (typeid(h) == typeid(c)) h = decltype(h)((uintptr_t)c);

namespace GW2LIB {
    class Agent;
    class Character;
    class Player;
    class Compass;
    class Gadget;
    class AttackTarget;
    class ResourceNode;

};


struct Gw2Hooks {
    bool(*AgCanBeSel)(bool&, GW2LIB::Agent &ag) = nullptr;
    void(*ChatHook)(wchar_t*) = nullptr;
    bool(*MouseMoveHook)(int x, int y, int modkeys) = nullptr;
    bool(*MouseButtonHook)(bool down, int button, int x, int y, int modkeys) = nullptr;
    bool(*MouseWheelHook)(int delta, int modkeys) = nullptr;
    void(*DmgLogHook)(GW2LIB::Agent &src, GW2LIB::Agent &tgt, int hit, GW2LIB::GW2::EffectType) = nullptr;
    void(*CombatLogHook)(GW2LIB::Agent &src , GW2LIB::Agent &tgt, int hit, GW2LIB::GW2::CombatLogType, GW2LIB::GW2::EffectType) = nullptr;
    void(*AllocatorHook)(int, size_t, int, int, char*) = nullptr;
    void(*LoggerHook)(char*) = nullptr;
};



class Gw2GameHook {
public:
    bool init_hooks();
    void cleanup();

    const hl::IHook *m_hkProcessText = nullptr;
    const hl::IHook *m_hkDmgLog = nullptr;
    const hl::IHook *m_hkCombatLog = nullptr;
    const hl::IHook *m_hkAllocator = nullptr;
    const hl::IHook *m_hkLogger = nullptr;
    const hl::IHook *m_hkLogger2 = nullptr;
    const hl::IHook *m_hkFrTxt = nullptr;
    const hl::IHook *m_hkMsgConn = nullptr;
    const hl::IHook *m_hkCanBeSel = nullptr;

    HHOOK m_hhkGetMessage = NULL;
    Gw2Hooks m_hookList;
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;

private:
    hl::Hooker m_hooker;
};


Gw2GameHook* get_hook();
Gw2Hooks* get_hook_list();

namespace GW2LIB {
    template<typename T>
    void SetGameHook(Gw2Hook type, T cb) {
        Gw2Hooks *list = get_hook_list();

        switch (type) {
        case HOOK_AG_CAN_BE_SEL: ASSIGN_HOOK(list->AgCanBeSel, cb); break;
        case HOOK_CHAT: ASSIGN_HOOK(list->ChatHook, cb); break;
        case HOOK_MOUSE_MOVE: ASSIGN_HOOK(list->MouseMoveHook, cb); break;
        case HOOK_MOUSE_BUTTON: ASSIGN_HOOK(list->MouseButtonHook, cb); break;
        case HOOK_MOUSE_WHEEL: ASSIGN_HOOK(list->MouseWheelHook, cb); break;
        case HOOK_DAMAGE_LOG: ASSIGN_HOOK(list->DmgLogHook, cb); break;
        case HOOK_COMBAT_LOG: ASSIGN_HOOK(list->CombatLogHook, cb); break;
        case HOOK_ALLOCATOR: ASSIGN_HOOK(list->AllocatorHook, cb); break;
        case HOOK_LOGGER: ASSIGN_HOOK(list->LoggerHook, cb); break;
        }
    }
};
