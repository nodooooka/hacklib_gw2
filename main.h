#ifndef GW2HACK_MAIN_H
#define GW2HACK_MAIN_H

#include "gw2lib.h"
#include "GameData.h"

#include "hacklib/Main.h"
#include "hacklib/ConsoleEx.h"
#include "hacklib/Hooker.h"
#include "hacklib/DrawerD3D.h"

#include <mutex>

#define HLGW2_EXCEPTION(msg) ExceptHandler(msg, GetExceptionCode(), GetExceptionInformation(), __FILE__, __FUNCTION__, __LINE__)

class Gw2HackMain *GetMain();
int64_t GetTimestamp();
DWORD ExceptHandler(const char*, DWORD, EXCEPTION_POINTERS, const char*, const char*, int);


#define CALL_FN(conv) ((T(conv*)(Ts...))m_ptr)(args...)
enum CallingConvention {
    CALL_CONV_FASTCALL,
    CALL_CONV_CDECL,
    CALL_CONV_STDCALL,
    CALL_CONV_THISCALL,
    CALL_CONV_VECTORCALL
};

template <typename T, CallingConvention cv = CALL_CONV_CDECL>
class ForeignFunction {
public:
    ForeignFunction() : m_ptr(nullptr) {}
    ForeignFunction(void *ptr) : m_ptr(ptr) {}
    ForeignFunction(uintptr_t ptr) { m_ptr = (void*)ptr; }

    template <typename... Ts>
    T operator()(Ts... args) {
        if (!m_ptr) throw EXCEPTION_ACCESS_VIOLATION;

        switch (cv) {
        case CALL_CONV_FASTCALL:
            return CALL_FN(__fastcall);
            break;

        case CALL_CONV_CDECL:
            return CALL_FN(__cdecl);
            break;

        case CALL_CONV_STDCALL:
            return CALL_FN(__stdcall);
            break;

        case CALL_CONV_THISCALL:
            return CALL_FN(__thiscall);
            break;

        case CALL_CONV_VECTORCALL:
            return CALL_FN(__vectorcall);
            break;

        default:
            throw EXCEPTION_ACCESS_VIOLATION;
        }
    }

    explicit operator bool() const {
        return !!m_ptr;
    }

private:
    void *m_ptr;

};


struct RenderState {
    D3DRENDERSTATETYPE state;
    DWORD value;
};

struct GamePointers
{
    int *pMapId = nullptr;
    int *pPing = nullptr;
    int *pFps = nullptr;
    int *pIfHide = nullptr;
    int *pMapOpen = nullptr;
    void *pAlertCtx = nullptr;
    void *pCtx = nullptr;
    void *pAgentViewCtx = nullptr;
    void **ppWorldViewContext = nullptr;
    void *pAgentSelectionCtx = nullptr;
    void *pCompass = nullptr;
    void *pUiOpts = nullptr;
    void *pCam = nullptr;
};


class Gw2HackMain : public hl::Main
{
public:
    bool init() override;
    void shutdown() override;

    const GamePointers *GetGamePointers() const { return &m_mems; }
    const GW2LIB::Mems *GetGameOffsets() const { return &m_pubmems; }

    hl::DrawerD3D *GetDrawer(bool bUsedToRender);
    const GameData::GameData *GetGameData() const;

    void SetRenderCallback(void (*cbRender)());

    void RenderHook(LPDIRECT3DDEVICE9 pDevice);
    void GameHook();

    const hl::IHook *m_hkPresent = nullptr;
    const hl::IHook *m_hkReset = nullptr;
    const hl::IHook *m_hkAlertCtx = nullptr;

    std::mutex m_gameDataMutex;
    Gw2GameHook m_gw2Hook;
    ForeignFunction<void*> GetContext;
    ForeignFunction<void*> GetCharacter; // void *GetCharacter(void *agent);
    ForeignFunction<void> LockCamera; // void LockCamera(void *wmAgent);
    ForeignFunction<void, CALL_CONV_THISCALL> LockCamera2;

private:
    void RefreshDataAgent(GameData::AgentData *pAgentData, hl::ForeignClass agent);
    void RefreshDataCharacter(GameData::CharacterData *pCharData, hl::ForeignClass character);
    void RefreshDataPlayer(GameData::PlayerData *pPlayerData, hl::ForeignClass player);
    void RefreshDataCompass(GameData::CompassData *pCompData, hl::ForeignClass comp);
    void RefreshDataGadget(GameData::GadgetData *pGadgetData, hl::ForeignClass gd);
    void RefreshDataAttackTarget(GameData::AttackTargetData *pGadgetTgtData, hl::ForeignClass gd);
    void RefreshDataResourceNode(GameData::ResourceNodeData *pRNodeData, hl::ForeignClass res);
    void RefreshDataBuff(GameData::BuffData *pBuffData, hl::ForeignClass buff);

    bool SetupCamData();
    bool SetupAgentArray();
    bool SetupCharacterArray();
    bool SetupPlayerArray();

    hl::ConsoleEx m_con;
    hl::Hooker m_hooker;
    hl::DrawerD3D m_drawer;

    GameData::GameData m_gameData;

    bool m_bPublicDrawer = false;
    void(*m_cbRender)() = nullptr;

    GamePointers m_mems;
    GW2LIB::Mems m_pubmems;

};

#endif